package th.co.integrationworks.iot.core;

import io.netpie.microgear.Microgear;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class CoreApplication {

    final static String appID = "MyAppz";
    final static String Key = "pwtJqzXpgENn3Nn";
    final static String Secret = "JNbl3bdRowF8RTHi15koLuSJ5";


    @Autowired
    private MicrogearAssist microgearAssist;

//    @Configuration
//    public static class Config {
//
//        @Bean
//        public MicrogearAssist MicrogearAssist() {
//            return new MicrogearAssist();
//        }
//
//    }

    public static void main(String[] args) {
        SpringApplication.run(CoreApplication.class, args);
    }

    @Bean
    public CommandLineRunner runner() {
        return (args) -> {
            try {
                microgearAssist.Connect(appID, Key, Secret);
                //microgearAssist.Subscribe("@push/owner");
//            int count = 1;
//            for(;;){
               microgearAssist.Publish("/@push/owner", "1" + ".  Test message");
//                count++;
//                Thread.sleep(2000);
//            }

                Thread.sleep(10000);
            } catch (Exception e) {

                e.printStackTrace();
            }
        };
    }

}