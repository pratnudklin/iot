package th.co.integrationworks.iot.core;

import io.netpie.microgear.Microgear;
import io.netpie.microgear.MicrogearEventListener;
import org.springframework.stereotype.Component;

@Component
public class MicrogearAssist extends Microgear implements MicrogearEventListener {

    public MicrogearAssist(){
       super();
       SetCallback(this);
    }

    @Override
    public void onConnect() {
        System.out.println("Connected.");
    }

    @Override
    public void onMessage(String topic, String message) {
        System.out.println(topic + " " + message);
    }

    @Override
    public void onPresent(String token) {
        System.out.println("Present " + token);
    }

    @Override
    public void onAbsent(String token) {
        System.out.println("Absent " + token);
    }

    @Override
    public void onDisconnect() {
        System.out.println("Disconnect.");
    }

    @Override
    public void onError(String error) {
        System.out.println("Error " + error);
    }

    @Override
    public void onInfo(String info) {
        System.out.println("Info " + info);
    }
}
